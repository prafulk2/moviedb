import React, { useState } from 'react'
import { NavLink } from 'react-router-dom'
import './Navbar.scss'
export default function Navbar() {
    const [isMobail, setisMobail] = useState(false)
    const setMobail = () => {
        setisMobail(!isMobail)
    }
    const setMobailFalse = () => {
        setisMobail(false)
    }
    return (
        <>
            <nav className="nav">
                <NavLink activeClassName="is_active" className="logo " to="/TopRateedMovies"  >
                        <img className="thumb image-max-width loaded" alt="navbar img" src="https://image.flaticon.com/icons/png/512/187/187851.png" width="40" height="40" />
                        <h1 className="logotext">Movie Wall </h1>
                </NavLink>

                <div className="icon">
                    <img src="https://img.icons8.com/nolan/64/menu.png" alt="icon" width="43" height="43" onClick={setMobail} />
                </div>

                <div className={isMobail ? "mobailScreen" : "navlinkdiv "} >
                    <ul className="nav-links" onClick={setMobailFalse}>
                        <NavLink activeClassName="is_active" className="Navlink " to="/TopRateedMovies"  >
                            <li>Top Rated Movies</li>
                        </NavLink>
                        <NavLink activeClassName="is_active" className="Navlink " to="/PopularMovie"  >
                            <li>Popular Movies</li>
                        </NavLink>
                        <NavLink activeClassName="is_active" className="Navlink " to="/UpComingMovie"  >
                            <li>Up Coming Movies</li>
                        </NavLink>
                    </ul>
                </div>
            </nav>
        </>
    )
}
