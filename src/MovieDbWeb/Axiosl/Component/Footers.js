import React from 'react'
import { Link, NavLink } from 'react-router-dom'
import './Footers.scss'
export default function Footers() {

    return (
        <>
            <div className="footer">
                <div className="outdiv">
                    <div className="contentdiv">
                        <NavLink activeClassName="is_active" className="logo " to="/TopRateedMovies"  >

                            <img className="thumb image-max-width loaded" data-pin-no-hover="true" src="https://image.flaticon.com/icons/png/512/187/187851.png" width="40" height="40" alt="Hollywood star neon sign Free Vector" data-real-image="https://image.freepik.com/free-vector/hollywood-star-neon-sign_1262-19560.jpg" data-src="https://img.freepik.com/free-vector/hollywood-star-neon-sign_1262-19560.jpg?size=338&amp;ext=jpg" />
                            <h1 className="logotext">Movie Wall </h1>
                        </NavLink>
                        <p className="contentp" >In a time of superstition and magic, when wolves are seen as demonic and nature an evil to be tamed, a young apprentice hunter comes to Ireland with her father to wipe out the last pack. But when she saves a wild native girl, their</p>
                    </div>
                    <div className="linkdiv">
                        <div className="outfdiv">
                            <div className="firstli comman">
                                <p>POPULAR MOVIE </p>
                                <ul className="foot-list">
                                    <li><Link to="/MovieDetails/588228">The Tomorrow War</Link></li>
                                    <li><Link to="/MovieDetails/451048">Jungle Cruise</Link></li>
                                    <li><Link to="/MovieDetails/619297">Sweet Girl</Link></li>
                                    <li><Link to="/MovieDetails/497698">Black Widow</Link></li>
                                    <li><Link to="/MovieDetails/550988">Free Guy</Link></li>
                                </ul>
                            </div>
                            <div className="secondli comman">
                                <p>TOP MOVIES</p>
                                <ul className="foot-list">
                                    <li><Link to="/MovieDetails/238">The Godfather</Link></li>
                                    <li><Link to="/MovieDetails/240">The Godfather: Part II</Link></li>
                                    <li><Link to="/MovieDetails/696374">Gabriel's Inferno</Link></li>
                                    <li><Link to="/MovieDetails/129">Spirited Away</Link></li>
                                    <li><Link to="/MovieDetails/526702">Black Beauty</Link></li>
                                </ul>
                            </div>
                        </div>
                        <div className="thirdeli comman">
                            <p>CONTACT US </p>
                            <ul className="foot-list">
                                <li>LinkedIn</li>
                                <li>Twitter</li>
                                <li>Facebook</li>
                                <li>We are Hiring!</li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div className="hr">
                    <hr />
                </div>
                <div className="Copyright">
                    <p> @{new Date().getFullYear()} Atom Pvt Ltd All Rights reserved</p>
                </div>
            </div>
        </>
    )
}
