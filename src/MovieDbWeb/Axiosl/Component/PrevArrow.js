import React from 'react'

export default function PrevArrow(props) {
    const { onClick } = props;
    return (
        <div
        className="prevArrow"
        onClick={onClick}
         style={{ height:"50px",width:"40px" }}>
            <img src="https://img.icons8.com/emoji/48/000000/left-arrow-emoji.png" alt="prearrimg"/>
        </div>
    )
}
