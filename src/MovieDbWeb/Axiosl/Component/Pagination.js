import React, { useState } from 'react'
import './Pagination.scss'
export default function Pagination({ paginate, currentPage, totalPage}) {
    
    //  use for mobail scrren
    const [firstIndM, setFirstIndM] = useState(0)
    const [lastIndM, setLastIndM] = useState(2)

    // use for leptop screen
    const [first, setFirst] = useState(0)
    const [lastindex, setLastindex] = useState(10)
   
    
    const current=[]
    for (let i = 1; i <= totalPage; i++) {
        current.push(i);
    }

    // for Leptop screen
    const pageNumber = current.slice(first,lastindex)
    const nextFun=()=>{
        if(lastindex<totalPage){
            setFirst(lastindex)
            setLastindex(lastindex+10)
        }
    }
    const previesfun=()=>{
        if(first!==0){
            setFirst(first-10)
            setLastindex(lastindex-10)
        }

    }

    // for Mobail screen
    const pageNumberM = current.slice(firstIndM,lastIndM)
    const nextFunMobail=()=>{
        if(lastIndM<totalPage){
            setFirstIndM(lastIndM)
            setLastIndM(lastIndM+2)
        }
    }
    const previesfunMobail=()=>{
        if(firstIndM!==0){
            setFirstIndM(firstIndM-2)
            setLastIndM(lastIndM-2)
        }

    }
    return (
        <>
            <nav className="paginations">
                <button className="btn btnNonMobail" onClick={previesfun}>Previes </button>
                <button className="btn btnOnMobail" onClick={previesfunMobail}>Previes </button>
                {/* for leptop screen */}
                <ul className="nav-link">
                    {pageNumber.map((number) => {
                        var activeClass = currentPage === number ? 'active' : '';
                        return <li key={number}>
                            <button href="#" className={`btn ${activeClass}`} onClick={() => paginate(number)} >
                                {number}
                            </button>
                        </li>
                    })
                    }
                </ul>
                {/* for mobail screen */}
                <ul className="nav-linkMobail">
                    {pageNumberM.map((number) => {
                        var activeClass = currentPage === number ? 'active' : '';
                        return <li key={number}>
                            <button href="#" className={`btn ${activeClass}`} onClick={() => paginate(number)} >
                                {number}
                            </button>
                        </li>
                    })
                    }
                </ul>
                <button className="btn btnNonMobail" onClick={nextFun}>Next </button>
                <button className="btn btnOnMobail" onClick={nextFunMobail}>Next </button>
            </nav>
        </>
    )
}
