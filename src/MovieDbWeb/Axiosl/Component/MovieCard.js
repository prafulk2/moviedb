import React from 'react'
import StarRatings from 'react-star-ratings'
import './MovieCard.scss'
import { Link } from 'react-router-dom'

export default function MovieCard(props) {

    const format = (dates) => {
        const date = new Date(dates);
        const month = ["Jan", "Feb", "Mar", "Apr", "May", "June", "July", "Aug", "Sep", "Oct", "Nov", "Dec"]
        return (date.getDate() + ' ' + month[date.getMonth()] + ' ' + date.getFullYear().toString().substr(-2));
    }

    return (
        <>

            <div className="cardContainer">
                <Link to={`/MovieDetails/${props.movieobj.id}`}>
                    <div className="cards">
                        <div className="imgdiv">
                            {console.log(props.movieobj.poster_path)}
                            {props.movieobj.poster_path===undefined?"loading": <img src={`https://image.tmdb.org/t/p/w500/${props.movieobj.poster_path}`} alt="" />}
                        </div>
                        <div className="cardbody">
                            <h2 className="title">{props.movieobj.title}</h2>
                        </div>
                        <div className="contentdivs">
                            <h4 className="release_date">
                                Release Date:<span> {format(props.movieobj.release_date)}</span>
                            </h4>
                            <h4 className="vote_count">
                                Vote Count:<span> {props.movieobj.vote_count}</span>
                            </h4>
                            <h4 className="rating">
                                Rating:<span>  <StarRatings rating={props.movieobj.vote_average / 2} starDimension="16px" starRatedColor="yellow" starEmptyColor="black" starSpacing="1px" numberOfStars={5} /></span>
                            </h4>
                        </div>
                    </div>
                </Link>
            </div>

        </>

    )
}
