import React, { useEffect, useState } from 'react'
import './MovieDetails.scss'
import StarRatings from 'react-star-ratings';
import { useParams } from 'react-router-dom';
import axios from 'axios'
import SimilarMovie from '../Pages/SimilarMovie'

export default function MovieDetails(props) {
    const params = useParams()

    // DetailMovies
    const [DetailMovie, setDetailMovie] = useState([]);

    const rate = DetailMovie?.vote_average;
    const rating = rate === undefined ? rate : rate / 2;


    // Atrrow function
    const Times = (minutes) => {
        const mixhour = minutes / 60;
        const cleanhour = Math.floor(mixhour)
        const mixminute = (mixhour - cleanhour) * 60
        const cleanminute = Math.round(mixminute)
        return cleanhour + " hours and " + cleanminute + " minutes"


    }


    const changeFormate = new Intl.NumberFormat('en-US', {
        style: 'currency',
        currency: 'USD',
    });

    useEffect(() => {
        axios.get(`https://api.themoviedb.org/3/movie/${params.id}?api_key=e46c5dc93c6a1a069a3af6ea4233bdad&language=en-US`)
            .then((res) => {
                setDetailMovie(res.data);
            }).catch((err) => {
                console.log(err);
            })

    }, [params])


    const format = (dates) => {
        if (dates !== undefined) {
            const date = new Date(dates);
            const month = ["Jan", "Feb", "Mar", "Apr", "May", "June", "July", "Aug", "Sep", "Oct", "Nov", "Dec"]
            return date.getDate() + ' ' + month[date.getMonth()] + ' ' + date.getFullYear().toString().substr(-2);
        }
        return undefined
    }

    useEffect(() => {
        window.scrollTo(0, 0)
    })
    return (
        <>
            <div className="MDetailContain">
                <div className="firstDiv">
                    <div className="maininnerdiv">
   
                        <div className="imgdiv">
                        {DetailMovie.poster_path!==undefined?
                        // {console.log(DetailMovie.poster_path)}
                        <img src={`https://image.tmdb.org/t/p/w500/${DetailMovie?.poster_path}`} alt="" />:<div style={{display:"flex",justifyContent:"center",width:"100%",height:"100%"}}><img src="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcR7PXCP4TxczFIhwmrIweeuchKgRXKd2e2XqQ&usqp=CAU" alt="loading"></img></div>}
                        </div>
                        <div className="contDiv">
                            <h3 className="title commanpadding">{DetailMovie?.title}</h3>
                            <p className="overview commanpadding">
                                {DetailMovie.overview}
                            </p>
                            <h4 className="vote_count commanpadding">
                                Original Title: <span>{DetailMovie?.original_title}.</span>
                            </h4>
                            <h4 className="vote_count commanpadding">
                                Tag Line: <span>{DetailMovie?.tagline}.</span>
                            </h4>
                            <h4 className="original_language commanpadding">
                                Original Language: <span>{DetailMovie?.original_language}</span>
                            </h4>
                            <h4 className="popularity commanpadding">
                                Popularity: <span>{DetailMovie?.popularity}</span>
                            </h4>
                            <h4 className="status commanpadding">
                                Status: <span>{DetailMovie?.status}</span>
                            </h4>
                            <h4 className="release_date commanpadding">
                                Release Date: <span>{format(DetailMovie?.release_date)}</span>
                            </h4>
                            <h4 className="vote_average commanpadding">
                                Ratings: <StarRatings rating={rating} starDimension="16px" starRatedColor="rgb(77, 129, 233)" starEmptyColor="black" starSpacing="0px" numberOfStars={5} />
                            </h4>
                            <h4 className="vote_count commanpadding">
                                Vote Count: <span> {DetailMovie?.vote_count}</span>
                            </h4>
                            <h4 className="budget commanpadding">
                                Budget: <span> {(DetailMovie?.budget) === undefined ? " " : changeFormate.format(DetailMovie?.budget)} </span>
                            </h4>
                            <h4 className="revenue commanpadding">
                                Revenue: <span> {DetailMovie?.revenue === undefined ? " " : changeFormate.format(DetailMovie?.revenue)} </span>
                            </h4>
                            <h4 className="belongs_to_collection commanpadding">
                                Belongs To Collection: <span> {DetailMovie.belongs_to_collection?.name}. </span>
                            </h4>
                            <h4 className="runtime commanpadding">
                                Runtime:<span> {DetailMovie.runtime===undefined?"":Times(DetailMovie.runtime)}. </span>
                            </h4>
                            <h4 className="production_countries commanpadding">
                                Production Countries: <span>{DetailMovie?.production_countries?.[0]?.name}.</span>
                            </h4>

                            <h4 className="production_countries commanpadding">
                                Spoken Languages: <span>{DetailMovie.spoken_languages?.map((val, id) => `${val.name}`).join(" ,")}.</span>
                            </h4>
                            <h4 className="production_countries commanpadding">
                                Genres: <span>{DetailMovie.genres?.map((val, id) => `${val.name}`).join(" ,")}.     </span>
                            </h4>
                        </div>
                    </div>
                </div>
                <div >
                    {DetailMovie===undefined?"loading...": <SimilarMovie />}
                </div>
            </div>
        </>
    )
}
