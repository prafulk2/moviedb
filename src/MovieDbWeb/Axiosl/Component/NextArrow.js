import React from 'react'

export default function NextArrow(props) {
    const { onClick } = props;
    return (
        <div
        className="nextArrow"
        onClick={onClick}
         style={{ height:"50px",width:"40px" }}>
            <img src="https://img.icons8.com/emoji/48/000000/right-arrow-emoji.png" alt="nextarrowimg"/>
        </div>
    )
}
