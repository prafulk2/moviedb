import React, { useEffect, useState } from 'react'
import axios from 'axios'
import MovieCard from '../Component/MovieCard'
import Pagination from '../Component/Pagination'
import './UpComingMovie.scss'

export default function UpComingMovie() {
    const [totalPage, setTotalPage] = useState(1)
    const [marr, setMarr] = useState([])
    // for search movie
    const [searchM, setSearchM] = useState("")
    // used for pagination
    const [currentPage, setCurrentPage] = useState(1)

    const [filarrr, setfilarrr] = useState(null)


    // console.log(marr);
    const fun1 = (e) => {
        setSearchM(e.target.value)
        if (e.target.value !== "" && e.target.value !== null) {
            return setfilarrr(marr.filter((val) => val.title.toLowerCase() === e.target.value.toLowerCase()).map((i) => i));
        }
        return setfilarrr(marr)
    }

    // call api based on Paggination cmp
    useEffect(() => {
        axios.get(`https://api.themoviedb.org/3/movie/upcoming?api_key=e46c5dc93c6a1a069a3af6ea4233bdad&language=en-US&page=${currentPage}`).then((res) => {
            setMarr(res.data.results);
            setTotalPage(res.data.total_pages)
            setfilarrr(res.data.results)
            // console.log(marr);
        })
            .catch((err) => {
                console.log(err);
            })
    }, [currentPage])

    // used for pagination
    const paginate = (pageNumber) => {
        setCurrentPage(pageNumber)
    }


    useEffect(() => {
        window.scrollTo(0, 0)
    })
    return (
        <>
            <div className="UpcomingContainer">
                <div className="innerbody">
                    <div className="search">
                        <input type="search" value={searchM} onChange={fun1} placeholder="Search Movie..." />
                    </div>
                    <div className="cont">
                        {filarrr!==null?filarrr.map((marrval, index) => <MovieCard movieobj={marrval} key={index} />):<div style={{display:"flex",justifyContent:"center",width:"100%"}}><img src="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcR7PXCP4TxczFIhwmrIweeuchKgRXKd2e2XqQ&usqp=CAU" alt="loading"></img></div>}
                    </div>
                    <Pagination paginate={paginate} currentPage={currentPage} totalPage={totalPage} />
                </div>
            </div>
        </>
    )
}