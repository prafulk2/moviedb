import React, { Suspense, useEffect } from 'react'
import { BrowserRouter as Router, Redirect, Route, Switch } from 'react-router-dom'
import Footers from '../Component/Footers'
import Navbar from '../Component/Navbar'
import './MovieMain.scss'

const TopRateedMovies = React.lazy(() => import('./TopRateedMovies'));
const PopularMovie = React.lazy(() => import('./PopularMovie'));
const UpComingMovie = React.lazy(() => import('./UpComingMovie'));
const MovieDetails = React.lazy(() => import('../Component/MovieDetails'));
const SimilarMovie = React.lazy(() => import('./SimilarMovie'));


export default function MovieMain(props) {
    useEffect(() => {
        window.scrollTo(0, 0)
    })
    return (
        <div >
            <div className="MovieMaincontainer">
                <Router onUpdate={() => window.scrollTo(0, 0)}>
                    <div className="header">
                        <Navbar />
                    </div>
                    <div className="maincontent">
                        {/* //looging image */}
                        <Suspense fallback={<div><img src="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcR7PXCP4TxczFIhwmrIweeuchKgRXKd2e2XqQ&usqp=CAU" alt="loading"></img></div>}>
                            <Switch>
                            <Route path="/MovieDetails/:id" component={MovieDetails} />
                                <Route exact path="/TopRateedMovies" component={TopRateedMovies} />
                                <Route path="/PopularMovie" component={PopularMovie} />
                                <Route path="/UpComingMovie" component={UpComingMovie} />
                               
                                <Route path="/SimilarMovie/:id" className="similarMovie" component={SimilarMovie} />
                                <Route exact path="/">
                                    <Redirect to="/TopRateedMovies" />
                                </Route>
                            </Switch>
                        </Suspense>
                    </div>

                    <div className="footer">
                        <Footers />
                    </div>
                </Router>
            </div>
        </div>
    )
}
