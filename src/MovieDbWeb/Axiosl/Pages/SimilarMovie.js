import React, { useEffect, useState } from 'react'
import { Link, useParams } from 'react-router-dom';
import StarRatings from 'react-star-ratings';
import PrevArrow from '../Component/PrevArrow';
import NextArrow from '../Component/NextArrow'
import axios from 'axios'
import './SimilarMovie.scss'
import Slider from "react-slick";
import "slick-carousel/slick/slick.css";
import "slick-carousel/slick/slick-theme.css";

export default function SimilarMovie(props) {
    const [similarMovies, setSimilarMovies] = useState([]);
    const smovieid = useParams()
    useEffect(() => {
        axios.get(`https://api.themoviedb.org/3/movie/${smovieid.id}/similar?api_key=e46c5dc93c6a1a069a3af6ea4233bdad&language=en-US&page=1`)
            .then((res) => {
                setSimilarMovies(res.data.results);
            }).catch((err) => {
                console.log(err);
            })
    }, [smovieid])

    const format = (dates) => {
        var date = new Date(dates);
        let month = ["Jan", "Feb", "Mar", "Apr", "May", "June", "July", "Aug", "Sep", "Oct", "Nov", "Dec"]
        return date.getDate() + ' ' + month[date.getMonth()] + ' ' + date.getFullYear().toString().substr(-2);
    }

    // Similar Movies var
    var settings = {
        fade: false,
        cssEase: 'linear',
        autoplay: true,
        autoplaySpeed: 5000,
        dots: false,
        infinite: true,
        speed: 1000,
        slidesToShow: 4,
        slidesToScroll: 1,
        arrows: true,
        prevArrow: <PrevArrow />,
        nextArrow: <NextArrow />,
        responsive: [
            {
                breakpoint: 1024,
                settings: {
                    slidesToShow: 4,
                    slidesToScroll: 1,
                    infinite: true,
                    dots: false
                }
            },
            {
                breakpoint: 769,
                settings: {
                    slidesToShow: 2,
                    slidesToScroll: 2,
                    initialSlide: 2
                }
            },
            {
                breakpoint: 480,
                settings: {
                    slidesToShow: 1,
                    slidesToScroll: 1,
                    dots: false
                }
            }
        ]
    };

    return (
        <>
            <div className="similarMContainer">
                {/* <Link to={`/SimilarMovie`}> */}
                <div className="mcards">
                    <Slider {...settings}>
                        {similarMovies.map((smmovie, index) => {
                            return  <div className="cardbodys" key="index">
                                        <Link to={`/MovieDetails/${smmovie.id}`}>
                                            <div className="card">
                                                <div className="imgdivs">
                                                    <img src={`https://image.tmdb.org/t/p/w500/${smmovie.poster_path}`} alt="" />
                                                </div>
                                                <div className="titles">
                                                    <h2 className="title">{smmovie.title}</h2>
                                                </div>
                                                <div className="contentdivs">
                                                    <h4 className="release_date">
                                                        Release Date:<span> {format(smmovie.release_date)}</span>
                                                    </h4>
                                                    <h4 className="vote_count">
                                                        Vote Count:<span> {smmovie.vote_count}</span>
                                                    </h4>
                                                    <h4 className="rating">
                                                        Rating:<span>  <StarRatings rating={smmovie.vote_average / 2} starDimension="16px" starRatedColor="yellow" starEmptyColor="black" starSpacing="1px" numberOfStars={5} /></span>
                                                    </h4>
                                                </div>
                                            </div>
                                        </Link>
                                    </div>
                            })
                        }
                    </Slider>
                </div>
            </div>
        </>
    )
}
