import React from 'react'
import Slider from "react-slick";
import "slick-carousel/slick/slick.css";
import "slick-carousel/slick/slick-theme.css";

export default function Demo() {
    var settings = {
        dots: true,
        infinite: true,
        speed: 500,
        slidesToShow: 3,
        slidesToScroll: 1
      };
      var arr=[1,2,3,5,6]
      return     <>
        <Slider {...settings}>
            {arr.map((val ,index)=>{
              return <div key={index}><h1>
                {val}
                </h1></div>
            })}
        </Slider>
      </>
    }
